// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDfUpLxnyQ2gcHeVZtJmsA_cQlxBUli8gc",
    authDomain: "workers-medina.firebaseapp.com",
    databaseURL: "https://workers-medina.firebaseio.com",
    projectId: "workers-medina",
    storageBucket: "workers-medina.appspot.com",
    messagingSenderId: "987959076005",
    appId: "1:987959076005:web:3d765b43fba35503fbd94c",
    measurementId: "G-ZGW6GBJZNH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
