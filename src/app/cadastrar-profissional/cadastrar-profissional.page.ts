import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '../model/user.model';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { UserService } from '../services/user.service';
import { CategoriasModel } from '../model/categorias.model';
import { CategoriasService } from '../services/categorias.service';

@Component({
  selector: 'app-cadastrar-profissional',
  templateUrl: './cadastrar-profissional.page.html',
  styleUrls: ['./cadastrar-profissional.page.scss'],
})
export class CadastrarProfissionalPage implements OnInit {

  user: UserModel = {};

  uid: string;

  loading: any;

  categorias: CategoriasModel[];

  qualificacao: string = '';
  diferencia_porem: string = '';
  categoria: string = 'Selecione...';
  categoriaSave: string = '';

  constructor(
    private router: Router,
    private loadingController: LoadingController,
    private userService: UserService,
    private navController: NavController,
    private toastController: ToastController,
    private categoriasService: CategoriasService
  ) {
    try {
      this.user = this.router.getCurrentNavigation().extras.state.user;
      this.uid = this.router.getCurrentNavigation().extras.state.uid;

      this.presentLoading().then(() => {
        this.userService.getUser(this.user.uid).subscribe((user) => {
          if (user.length != 0) {
            this.user = user[0];
          }
          this.categoriasService.getCategorias().subscribe((categorias) => {
            this.categorias = categorias;
            this.loading.dismiss();
          });
        })
      });
    } catch (err) {
      console.log(err);
      this.navController.navigateRoot(['splash']);
    }
  }

  selectChange(event) {
    this.categoriaSave = event.srcElement.value;
  }

  addCategoria(event: any) {
    this.presentLoading().then(() => {
      this.user.dados_trabalhador.categorias.push(this.categoriaSave);
      this.categoriaSave = '';
      this.userService.addCategoria(this.uid, this.user).then((s) => {
        this.presentToast("Salvo!", "Categoria Salva com sucesso");
        this.loading.dismiss();
      }).catch((e) => {
        this.presentToast("Erro ao salvar categoria", e.message);
        this.loading.dismiss();
      })
    });
  }

  removeCategoria(i: number, categoria: string) {
    this.presentLoading().then(() => {
      this.user.dados_trabalhador.categorias.splice(i, 1);
      this.userService.addCategoria(this.uid, this.user).then((s) => {
        this.presentToast("Salvo!", "Categoria removica com sucesso");
        this.loading.dismiss();
      }).catch((e) => {
        this.presentToast("Erro ao remover categoria", e.message);
        this.loading.dismiss();
      })
    });
  }

  addQualificacao(event: any) {
    this.presentLoading().then(() => {
      this.user.dados_trabalhador.qualificacoes.push(this.qualificacao);
      this.qualificacao = '';
      this.userService.addQualificacao(this.uid, this.user).then((s) => {
        this.presentToast("Salvo!", "Qualificação Salva com sucesso");
        this.loading.dismiss();
      }).catch((e) => {
        this.presentToast("Erro ao salvar qualificação", e.message);
        this.loading.dismiss();
      })
    });
  }

  removeQualificao(i: number, qualificacao: string) {
    this.presentLoading().then(() => {
      this.user.dados_trabalhador.qualificacoes.splice(i, 1);
      this.userService.addQualificacao(this.uid, this.user).then((s) => {
        this.presentToast("Salvo!", "Qualificação removica com sucesso");
        this.loading.dismiss();
      }).catch((e) => {
        this.presentToast("Erro ao remover qualificação", e.message);
        this.loading.dismiss();
      })
    });
  }

  addDifPor(event: any) {
    this.presentLoading().then(() => {
      this.user.dados_trabalhador.diferenciais_porens.push(this.diferencia_porem);
      this.diferencia_porem = '';
      this.userService.addDifPor(this.uid, this.user).then((s) => {
        this.presentToast("Salvo!", "Diferencial/Porém Salva com sucesso");
        this.loading.dismiss();
      }).catch((e) => {
        this.presentToast("Erro ao salvar diferencial/porém", e.message);
        this.loading.dismiss();
      })
    });
  }

  removeDifPor(i: number, dif_por: string) {
    this.presentLoading().then(() => {
      this.user.dados_trabalhador.diferenciais_porens.splice(i, 1);
      this.userService.addDifPor(this.uid, this.user).then((s) => {
        this.presentToast("Salvo!", "Diferencial/Porém removica com sucesso");
        this.loading.dismiss();
      }).catch((e) => {
        this.presentToast("Erro ao remover diferencial/porém", e.message);
        this.loading.dismiss();
      })
    });
  }

  ngOnInit() {
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Carregando...",
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

  async presentToast(head: string, msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 5000,
      closeButtonText: "OK",
      showCloseButton: true,
      mode: "ios",
      position: "bottom",
      header: head
    });
    toast.present();
  }


}
