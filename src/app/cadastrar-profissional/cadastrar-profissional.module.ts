import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastrarProfissionalPageRoutingModule } from './cadastrar-profissional-routing.module';

import { CadastrarProfissionalPage } from './cadastrar-profissional.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastrarProfissionalPageRoutingModule
  ],
  declarations: [CadastrarProfissionalPage]
})
export class CadastrarProfissionalPageModule {}
