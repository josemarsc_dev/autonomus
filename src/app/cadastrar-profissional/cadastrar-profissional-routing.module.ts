import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastrarProfissionalPage } from './cadastrar-profissional.page';

const routes: Routes = [
  {
    path: '',
    component: CadastrarProfissionalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastrarProfissionalPageRoutingModule {}
