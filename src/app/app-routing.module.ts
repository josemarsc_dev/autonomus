import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'splash', loadChildren: () => import('./splash/splash.module').then(m => m.SplashPageModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule) },
  { path: 'register', loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule) },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  { path: 'profissionais-categoria', loadChildren: () => import('./dashboard/profissionais-categoria/profissionais-categoria.module').then(m => m.ProfissionaisCategoriaPageModule) },
  { path: 'detalhes-profissional', loadChildren: () => import('./dashboard/detalhes-profissional/detalhes-profissional.module').then(m => m.DetalhesProfissionalPageModule) },
  { path: 'cadastrar-profissional', loadChildren: () => import('./cadastrar-profissional/cadastrar-profissional.module').then(m => m.CadastrarProfissionalPageModule) },
  { path: 'chat', loadChildren: () => import('./dashboard/chat/chat.module').then(m => m.ChatPageModule) }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
