import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/model/user.model';
import { Router } from '@angular/router';
import { CategoriasModel } from 'src/app/model/categorias.model';
import { UserService } from 'src/app/services/user.service';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-profissionais-categoria',
  templateUrl: './profissionais-categoria.page.html',
  styleUrls: ['./profissionais-categoria.page.scss'],
})
export class ProfissionaisCategoriaPage implements OnInit {

  filteredProfissionais: UserModel[] = [];
  profissionais: UserModel[] = [];

  categoria: CategoriasModel;

  user: UserModel;
  uid: string;

  loading: any;

  constructor(
    private router: Router,
    private navController: NavController,
    private userService: UserService,
    private loadingController: LoadingController
  ) {
    try {
      this.categoria = this.router.getCurrentNavigation().extras.state.categoria;
      this.user = this.router.getCurrentNavigation().extras.state.user;

      this.uid = this.user['id'];

      this.presentLoading().then(() => {
        this.userService.getUsersByCategoria(this.categoria).subscribe((users) => {
          if (users.length != 0) {
            users.forEach(user => {
              if (user.uid != this.user.uid) {
                this.profissionais.push(user);
              }
            });
            this.filteredProfissionais = this.profissionais;
          }
          this.loading.dismiss();
        })
      });
    } catch {
      this.navController.navigateRoot(['splash']);
    }
  }

  detalhesProfissional(profissional: UserModel) {
    this.navController.navigateForward(['detalhes-profissional'], { state: { profissional: profissional, user: this.user } });
  }

  mandarMensagem(profissional: UserModel) {
    console.log(profissional);
  }

  ngOnInit() {
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Carregando...",
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

}
