import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfissionaisCategoriaPage } from './profissionais-categoria.page';

const routes: Routes = [
  {
    path: '',
    component: ProfissionaisCategoriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfissionaisCategoriaPageRoutingModule {}
