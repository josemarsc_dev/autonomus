import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfissionaisCategoriaPageRoutingModule } from './profissionais-categoria-routing.module';

import { ProfissionaisCategoriaPage } from './profissionais-categoria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfissionaisCategoriaPageRoutingModule
  ],
  declarations: [ProfissionaisCategoriaPage]
})
export class ProfissionaisCategoriaPageModule {}
