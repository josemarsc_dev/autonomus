import { Component, OnInit } from '@angular/core';
import { UserModel } from '../model/user.model';
import { UserService } from '../services/user.service';
import { LoadingController, NavController } from '@ionic/angular';
import { StorageUserService } from '../services/storage-user.service';
import { CategoriasModel } from '../model/categorias.model';
import { CategoriasService } from '../services/categorias.service';
import { Router, Routes } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  user: UserModel;
  user_id: string;
  uid: string;
  state: any;

  loading: any;

  itensMenu: { icon: string, name: string, url?: string }[] = [
    { icon: 'home', name: 'Home', url: 'dashboard' }
  ]

  categorias: CategoriasModel[] = [];
  filteredCategorias: CategoriasModel[] = [];

  searchbar_hidden: boolean = false;
  queryText: string = '';

  constructor(
    private userService: UserService,
    private storageUser: StorageUserService,
    private loadingController: LoadingController,
    private navController: NavController,
    private categoriasService: CategoriasService,
    private router: Router,
  ) {
    this.presentLoading('Carregando...').then(() => {
      this.storageUser.get('user_id').then((uid) => {
        if (uid) {
          this.user_id = uid;
          this.userService.getUser(this.user_id).subscribe((user) => {
            this.user = user[0];
            this.uid = user[0]['id'];
            if (!this.user.dados_trabalhador) {
              this.itensMenu.push({ icon: 'people', name: 'Cadastrar como profissional', url: 'cadastrar-profissional' });
            } else {
              this.itensMenu.push(
                { icon: 'people', name: 'Editar meus dados profissionis', url: 'cadastrar-profissional' }
              );
            }
            this.itensMenu.push({ icon: 'exit', name: 'Sair', url: 'sair' });
            this.presentLoading('Carregando Categorias. Aguarde...').then(() => {
              this.categoriasService.getCategorias().subscribe((categorias) => {
                this.categorias = categorias;
                this.filteredCategorias = this.categorias;
                this.loading.dismiss()
              })
            })
            this.loading.dismiss();
          });
        } else {
          this.navController.navigateRoot(['login']);
          this.loading.dismiss();
        }
      })
    });
  }

  ngOnInit() {
  }

  profissionaisCategoria(categoria: CategoriasModel) {
    this.navController.navigateForward(['./profissionais-categoria'], { state: { categoria: categoria, user: this.user } });
  }

  onBlur(event: any) {
    if (event.srcElement.value == '') {
      this.filteredCategorias = this.categorias;
    }
  }

  filterCat(event) {
    let val = event.srcElement.value;
    this.filteredCategorias = [];
    this.categorias.forEach(cat => {
      if (typeof cat.nome == "string") {
        if (cat.nome.toLowerCase().includes(val.toLowerCase())) {
          this.filteredCategorias.push(cat);
        }
      }
    })
  }

  toggleSearch(event) {
    console.log(event);
    this.searchbar_hidden = !this.searchbar_hidden;
    if (!this.searchbar_hidden) {
      this.queryText = '';
      this.filteredCategorias = this.categorias;
    }
  }

  onClickMenu(item: { icon: string, name: string, url?: string }) {
    switch (item.url) {
      case 'sair':
        this.storageUser.remove('user_id').then(() => {
          this.navController.navigateRoot(['splash']);
        });
        break;
      case 'cadastrar-profissional':
        this.navController.navigateForward(['cadastrar-profissional'], { state: { user: this.user, uid: this.uid } });
        break;

      default:
        break;
    }
  }

  async presentLoading(msg: string) {
    this.loading = await this.loadingController.create({
      message: msg,
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

}
