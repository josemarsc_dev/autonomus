import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/model/user.model';
import { Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-detalhes-profissional',
  templateUrl: './detalhes-profissional.page.html',
  styleUrls: ['./detalhes-profissional.page.scss'],
})
export class DetalhesProfissionalPage implements OnInit {

  profissional: UserModel = {};
  user: UserModel = {};
  uid: string = '';

  user_id: string;
  profissional_id: string;

  loading: any;

  constructor(
    private router: Router,
    private loadingController: LoadingController,
    private userService: UserService,
    private navController: NavController
  ) {
    try {
      this.profissional = this.router.getCurrentNavigation().extras.state.profissional;
      this.user = this.router.getCurrentNavigation().extras.state.user;

      this.user_id = this.user['id'];
      this.profissional_id = this.profissional['id'];

      this.presentLoading().then(() => {
        this.userService.getUser(this.profissional.uid).subscribe((user) => {
          this.profissional = user[0];
          this.loading.dismiss();
        })
      });
    } catch {
      this.navController.navigateRoot(['splash']);
    }
  }

  ngOnInit() {
  }

  chat(profissional) {
    this.navController.navigateForward(['chat'], { state: { sender_id: this.user_id, receiver_id: this.profissional_id } });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Carregando...",
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }
}
