import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { LoadingController, NavController, IonContent } from '@ionic/angular';
import { UserModel } from 'src/app/model/user.model';
import { MessageModel } from 'src/app/model/message.model';
import { MessagesService } from 'src/app/services/messages.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  receiver: UserModel = {};
  sender: UserModel = {};

  sender_id: string;
  receiver_id: string;

  messageSend: string = '';
  m: any;
  messages: MessageModel[];
  message: MessageModel;

  loading: any;
  chatUid: string;

  constructor(
    private router: Router,
    private userService: UserService,
    private loadingController: LoadingController,
    private navController: NavController,
    private messagesService: MessagesService
  ) {
    try {
      this.sender_id = this.router.getCurrentNavigation().extras.state.sender_id;
      this.receiver_id = this.router.getCurrentNavigation().extras.state.receiver_id;

      this.presentLoading().then(() => {
        this.userService.getUserById(this.receiver_id).subscribe((receiver) => {
          console.log(receiver);
          this.receiver = receiver;
          this.presentLoading().then(() => {
            this.userService.getUserById(this.sender_id).subscribe((user) => {
              console.log(user);
              this.sender = user;
              this.messagesService.getConversation(this.sender_id, this.receiver_id).subscribe((conv) => {
                if (conv.length != 0) {
                  this.chatUid = conv[0].id;
                  this.messagesService.getMessages(conv[0].id).subscribe((messages) => {
                    this.messages = messages;
                  });
                }
              });
              this.loading.dismiss();
            })
          });
          this.loading.dismiss();
        })
      });
    } catch (error) {
      this.navController.navigateRoot(['splash']);
      console.log(error);
    }
  }

  sendMessage() {
    this.message = {
      participants: [this.sender_id, this.receiver_id],
      content: this.messageSend,
      senderUid: this.sender_id,
      senderNome: this.sender.nome,
      receiverUid: this.receiver_id,
      receiverNome: this.receiver.nome,
      timestamp: Date.now()
    };
    this.messageSend = '';
    this.messagesService.updateConversation(this.chatUid, this.message).then((v) => {
      console.log(v);
    });
  }

  ngOnInit() {
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Carregando...",
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

}
