import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { StorageUserService } from '../services/storage-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  senha: string;

  loading: any;
  toast: any;

  constructor(
    private loadingController: LoadingController,
    private authService: AuthService,
    private toastController: ToastController,
    private navController: NavController,
    private storageUser: StorageUserService
  ) { }

  ngOnInit() {
  }

  login() {
    this.presentLoading().then(() => {
      this.authService.login(this.email, this.senha).then((value) => {
        if (value) {
          this.storageUser.save('user_id', value.user.uid).then((value) => {
            this.loading.dismiss();
            this.navController.navigateRoot(['dashboard']);
          });
        } else {
          this.presentToast("Erro desconhecido", value);
          console.log("erro", value);
        }
      }).catch((reason) => {
        this.loading.dismiss();
        this.presentToast("Erro", reason.message);
        console.log("REASON", reason);
      });
    });
  }

  register() {
    this.navController.navigateForward(['register'], { state: { "email": this.email ? this.email : null } });
  }

  recover() {
    console.log("recover");
  }

  async presentToast(header: string, msg: string) {
    this.toast = await this.toastController.create({
      message: msg,
      duration: 5000,
      header: header,
      mode: "ios",
      position: "bottom",
      showCloseButton: true,
      closeButtonText: "OK"
    });
    this.toast.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Entrando...",
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

}
