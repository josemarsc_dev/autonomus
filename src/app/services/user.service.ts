import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { UserModel } from '../model/user.model';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CategoriasModel } from '../model/categorias.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usersCollection: AngularFirestoreCollection<UserModel>;
  query: AngularFirestoreCollection<UserModel>;
  users: Observable<UserModel[]>;
  user: UserModel;

  constructor(private angularFirestore: AngularFirestore) {
    this.usersCollection = this.angularFirestore.collection<UserModel>('users');
    this.users = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getUsers(): Observable<UserModel[]> {
    return this.users;
  }

  getUsersByCategoria(categoria: CategoriasModel): Observable<UserModel[]> {
    return this.angularFirestore.collection<UserModel>('users', ref => ref.where('dados_trabalhador.categorias', 'array-contains', categoria.nome)).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    );
  }

  getUserById(uid: string): Observable<UserModel | any> {
    return this.angularFirestore.collection<UserModel>('users').doc(uid).snapshotChanges().pipe(
      map(actions => {
        const data = actions.payload.data();
        const id = actions.payload.id;
        return { id, ...data };
      })
    )
  }

  getUser(user_id: string): Observable<UserModel[]> {
    return this.angularFirestore.collection<UserModel>('users', ref => ref.where('uid', '==', user_id)).snapshotChanges().pipe(
      take(1),
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    );
  }

  addCategoria(uid: string, user: UserModel): Promise<void> {
    return this.usersCollection.doc(uid).set(user);
  }

  addQualificacao(uid: string, user: UserModel): Promise<void> {
    return this.usersCollection.doc(uid).set(user);
  }

  addDifPor(uid: string, user: UserModel) {
    return this.usersCollection.doc(uid).set(user);
  }

  cadastrarUsuario(user: UserModel): Promise<DocumentReference> {
    return this.usersCollection.add(user);
  }

}
