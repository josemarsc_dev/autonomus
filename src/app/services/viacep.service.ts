import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ViacepService {

  constructor(private http: Http) { }

  getAddress(cep: string): Observable<any> {
    try {
      const url = "https://viacep.com.br/ws/" + cep + "/json/";
      const params = {};
      return this.http.get(url, params);
    } catch (error) {
      console.error(error.status);
      console.error(error.error); // Error message as string
      console.error(error.headers);
    }
  }
}
