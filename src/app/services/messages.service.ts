import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageModel } from '../model/message.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private angularFirestore: AngularFirestore) { }

  getConversation(sender: string, receiver: string): Observable<any> {
    console.log(sender, receiver);
    return this.angularFirestore.collection<MessageModel>('messages',
      ref => ref.where('participants', 'array-contains-any', [sender, receiver]))
      .snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        })
      );
  }

  getMessages(idConversation): Observable<MessageModel[]> {
    return this.angularFirestore.collection('messages').doc(idConversation).collection<MessageModel>('mess', ref => ref.orderBy('timestamp')).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  updateConversation(id: string, message: MessageModel): Promise<any> {
    if (id) {
      return this.angularFirestore.collection('messages').doc(id).collection<MessageModel>('mess').add(message);
    } else {
      let me = {
        sender: message.senderUid,
        receiver: message.receiverUid,
        participants: message.participants
      };
      return this.angularFirestore.collection('messages').add(me).then((val) => {
        val.collection('mess').add(message);
      });
    }
  }
}
