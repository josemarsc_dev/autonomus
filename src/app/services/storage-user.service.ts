import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageUserService {

  constructor(private storage: Storage) { }

  save(key: string, value: string): Promise<any> {
    return this.storage.set(key, value);
  }

  get(key: string): Promise<string> {
    return this.storage.get(key);
  }

  remove(key: string): Promise<string> {
    return this.storage.remove(key);
  }
}
