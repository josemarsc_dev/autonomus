import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { CategoriasModel } from '../model/categorias.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  categoriasCollection: AngularFirestoreCollection<CategoriasModel>;
  categorias: Observable<CategoriasModel[]>;
  categoria: CategoriasModel;

  constructor(
    private angularFirestore: AngularFirestore
  ) {
    this.categoriasCollection = this.angularFirestore.collection<CategoriasModel>('categorias');
    this.categorias = this.categoriasCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getCategorias(): Observable<CategoriasModel[]> {
    return this.angularFirestore.collection<CategoriasModel>('categorias').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    );
  }
}
