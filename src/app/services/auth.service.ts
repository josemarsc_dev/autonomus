import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { UserCredential } from '@firebase/auth-types';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private angularFireAuth: AngularFireAuth) { }

  login(email: string, senha: string): Promise<any> {
    console.log(email, senha);
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, senha);
  }

  signUp(email: string, password: string): Promise<UserCredential> {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password).then((user) => {
      return user;
    })
  }
}
