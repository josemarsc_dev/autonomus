import { Component, OnInit } from '@angular/core';
import { UserModel } from '../model/user.model';
import { StorageUserService } from '../services/storage-user.service';
import { LoadingController, NavController } from '@ionic/angular';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  loading: any;

  constructor(
    private storageUser: StorageUserService,
    private loadingController: LoadingController,
    private userService: UserService,
    private navController: NavController
  ) { }

  ngOnInit() {
    this.presentLoading().then(() => {
      this.verifyUser().then((value) => {
        if (value) {
          this.userService.getUser(value).subscribe((user) => {
            if (user) {
              console.log(user);
              this.loading.dismiss();
              this.navController.navigateRoot(['dashboard']);
            }
          });
        } else {
          this.loading.dismiss();
          this.navController.navigateRoot(['login']);
        }
      })
    });
  }

  async verifyUser(): Promise<string> {
    return this.storageUser.get("user_id").then((value) => {
      return value ? value : null;
    }).catch((reason) => {
      console.log("Ocorreu um erro ao iniciar o aplicativo. Tente novamente", reason);
      return null;
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Carregando...",
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

}
