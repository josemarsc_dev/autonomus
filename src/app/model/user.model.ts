import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserModel {

  public uid?: string;
  public nome?: string;
  public sobrenome?: string;
  public email?: string;
  public telefone?: string;
  public imgProfile?: string;
  public endereco?: {
    cep: string,
    cidade: string,
    bairro: string,
    rua: string,
    numero: number,
    referencia: string,
  };
  public dados_trabalhador?: {
    categorias?: string[]
    qualificacoes?: string[];
    diferenciais_porens?: string[];
  };

  constructor() { }
}
