import { Injectable } from '@angular/core';
import { UserModel } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class MessageModel {

  public participants: string[]
  public senderUid: string;
  public senderNome: string;
  public receiverUid: string;
  public receiverNome: string;
  public timestamp: number;
  public content: string;

  constructor() { }
}
