import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserModel } from '../model/user.model';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { LoadingController, AlertController, NavController, ToastController } from '@ionic/angular';
import { ViacepService } from '../services/viacep.service';
import { StorageUserService } from '../services/storage-user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: UserModel = {};

  loading: any;
  alert: any;

  email: string;
  selectedSegment: string = 'dadospessoais';
  cadFormDadosPessoais: FormGroup;
  cadFormEndereco: FormGroup;

  dadosPessoaisPreenchidos: boolean = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private loadginController: LoadingController,
    private alertController: AlertController,
    private navController: NavController,
    private toastController: ToastController,
    private viacepService: ViacepService,
    private storageUserService: StorageUserService,
  ) {
    if (this.router.getCurrentNavigation()) {
      if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.email) {
        this.email = this.router.getCurrentNavigation().extras.state.email;
      }
    }
  }

  ngOnInit() {
    this.cadFormDadosPessoais = this.formBuilder.group({
      nome: ['', Validators.required],
      sobrenome: ['', Validators.required],
      telefone: ['', Validators.required],
      email: [this.email ? this.email : '', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(6)]],
      confirmasenha: ['', [Validators.required, Validators.minLength(6)]]
    });

    this.cadFormEndereco = this.formBuilder.group({
      // cep: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      cidade: ['Teófilo Otoni', Validators.required],
      bairro: ['', Validators.required],
      rua: ['', Validators.required],
      numero: ['', Validators.required],
      referencia: ['']
    });
  }

  onSubmitDadosPessoais() {
    this.presentLoading("Verificando disponibilidade...").then(() => {
      this.authService.signUp(this.cadFormDadosPessoais.value.email, this.cadFormDadosPessoais.value.senha).then((userFirebase) => {
        console.log("in here", userFirebase);
        if (userFirebase.user) {
          this.user.uid = userFirebase.user.uid;
          this.user.nome = this.cadFormDadosPessoais.value.nome;
          this.user.sobrenome = this.cadFormDadosPessoais.value.sobrenome;
          this.user.email = userFirebase.user.email;
          this.user.telefone = this.cadFormDadosPessoais.value.telefone;

          this.dadosPessoaisPreenchidos = true;
          this.selectedSegment = 'endereco';

          this.loading.dismiss();
        } else {
          console.log(userFirebase);
        }
      }).catch((reason) => {
        this.loading.dismiss();
        switch (reason.code) {
          case 'auth/email-already-in-use':
            this.presentAlertEmailJaCadastrado();
            break;
        }
      })
    });
  }

  onSubmitEndereco() {
    if (this.cadFormEndereco.value.cidade != "Teófilo Otoni") {
      this.presentAlertCidadeInvalida();
    } else {
      this.presentLoading("Salvando dados...").then(() => {
        this.user.endereco = this.cadFormEndereco.value;
        this.userService.cadastrarUsuario(this.user).then((res) => {
          this.storageUserService.save('user_id', res.id).then((value) => {
            this.loading.dismiss();
            this.presentToast("Você pode realizar login com o novo usuário agora");
            this.navController.pop();
          })
        })
      });
    }
  }

  segmentChanged(event: any) {
    this.selectedSegment = event.srcElement.value;
  }

  //Dando erro de CORS em getAddress()
  inputOnCep(event: any) {
    // if (event.srcElement.value.length == 8) {
    //   this.presentLoading("Procurando CEP...").then(() => {
    //     console.log(this.cadFormEndereco);
    //     this.viacepService.getAddress(this.cadFormEndereco.value.cep).subscribe((res) => {
    //       console.log(res);
    //       // this.cadFormEndereco.value.cidade = res.localidade;
    //       // this.cadFormEndereco.value.bairro = res.bairro;
    //       // this.cadFormEndereco.value.rua = res.logradouro;
    //       this.loading.dismiss();
    //     });
    //   });
    // }
  }

  presentAlertCidadeInvalida() {
    let bts = [
      {
        role: "cancel",
        text: "OK",
        handler: () => null
      }
    ];
    this.presentAlert("Cidade não suportada", "Estamos em operação apenas em Teófilo Otoni por enquanto. Em breve estaremos expandindo", bts);
  }

  presentAlertEmailJaCadastrado() {
    let bts = [
      {
        role: "cancel",
        text: "Tentar email diferente",
        handler: () => null
      },
      {
        role: null,
        text: "Fazer Login",
        handler: () => {
          this.navController.navigateRoot(['login'])
        }
      }
    ];
    this.presentAlert(
      "e-mail já em uso", "O e-mail digitado já está sendo usado por outro usuário, o que deseja fazer?", bts);
  }

  async presentAlert(subheader: string, msg: string, buttons: { text: string, role?: string, handler?: any }[]) {
    this.alert = await this.alertController.create({
      message: msg,
      backdropDismiss: false,
      mode: "ios",
      buttons: buttons,
      header: "Aviso",
      subHeader: subheader
    });
    this.alert.present();
  }

  async presentLoading(msg: string) {
    this.loading = await this.loadginController.create({
      message: msg,
      backdropDismiss: false,
      mode: "ios",
      spinner: "lines"
    });
    this.loading.present();
  }

  async presentToast(msg: string) {
    await this.toastController.create({
      closeButtonText: "OK",
      duration: 4000,
      message: msg,
      showCloseButton: true,
      position: "bottom",
      mode: "ios",
      color: "primary"
    })
  }

}
